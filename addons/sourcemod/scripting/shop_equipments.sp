#pragma semicolon 1

#include <sdktools>
#include <sdkhooks>
#include <shop>
#include <tp_preview>
int g_iOwner[2048];

#define PLUGIN_VERSION	"2.2.140"

Handle g_hLookupAttachment = null;

KeyValues kv;

Handle hTrieEntity[MAXPLAYERS+1];
Handle hTrieItem[MAXPLAYERS+1];
//Handle hTimer[MAXPLAYERS+1];
char g_sClLang[MAXPLAYERS+1][3];

Handle hCategories;
bool g_bInPreview[MAXPLAYERS+1];

Handle g_hPreview;
bool g_bPreview;
Handle g_hRemoveOnDeath;
bool g_bRemoveOnDeath;

//#include <profiler>
//Handle g_hProf;

public Plugin:myinfo =
{
    name        = "[Shop] Equipments",
    author      = "FrozDark",
    description = "Equipments component for shop",
    version     = PLUGIN_VERSION,
    url         = "www.hlmod.ru"
};

public OnPluginStart()
{
	//g_hProf = CreateProfiler();
	Handle hGameConf = LoadGameConfigFile("shop_equipments.gamedata");
	if (hGameConf == null)
		SetFailState("gamedata/\"shop_equipments.gamedata.txt\" not found");
	StartPrepSDKCall(SDKCall_Entity);
	PrepSDKCall_SetFromConf(hGameConf, SDKConf_Signature, "LookupAttachment");
	PrepSDKCall_SetReturnInfo(SDKType_PlainOldData, SDKPass_Plain);
	PrepSDKCall_AddParameter(SDKType_String, SDKPass_Pointer);
	if ((g_hLookupAttachment = EndPrepSDKCall()) == null)
		SetFailState("Could not get \"LookupAttachment\" signature");
	CloseHandle(hGameConf);
	
	HookEvent("player_spawn", Event_PlayerSpawn);
	HookEvent("player_death", Event_PlayerDeath, EventHookMode_Pre);
	HookEvent("player_team", Event_player_team, EventHookMode_Pre);

	hCategories = CreateArray(ByteCountToCells(64));
	
	RegAdminCmd("equipments_reload", Command_Reload, ADMFLAG_ROOT, "Reloads equipments configuration");
	
	g_hPreview = CreateConVar("sm_shop_equipments_preview", "1", "Enables preview for equipments");
	g_bPreview = GetConVarBool(g_hPreview);
	HookConVarChange(g_hPreview, OnConVarChange);
	
	g_hRemoveOnDeath = CreateConVar("sm_shop_equipments_remove_on_death", "1", "Removes a player's equipments on death");
	g_bRemoveOnDeath = GetConVarBool(g_hRemoveOnDeath);
	HookConVarChange(g_hRemoveOnDeath, OnConVarChange);
	
	AutoExecConfig(true, "shop_equipments", "shop");
	
	StartPlugin();
	
}

public OnConVarChange(Handle:convar, const String:oldValue[], const String:newValue[])
{
	if (convar == g_hPreview)
		g_bPreview = bool:StringToInt(newValue);
	else if (convar == g_hRemoveOnDeath)
		g_bRemoveOnDeath = bool:StringToInt(newValue);
}

StartPlugin()
{
	for (int i=1; i<=MaxClients; i++)
		if (IsClientConnected(i)) {
			OnClientConnected(i);
			if (IsClientInGame(i))
				OnClientPutInServer(i);
		}
	if (Shop_IsStarted()) Shop_Started();
}

public OnPluginEnd()
{
	Shop_UnregisterMe();
	for (int i=1; i<=MaxClients; i++)
		OnClientDisconnect(i);

}

public Shop_Started()
{
	if (kv != null)
		CloseHandle(kv);
	
	kv = CreateKeyValues("Equipments");
	
	decl String:_buffer[PLATFORM_MAX_PATH];
	Shop_GetCfgFile(_buffer, sizeof(_buffer), "equipments.txt");
	
	if (!FileToKeyValues(kv, _buffer))
		SetFailState("Couldn't parse file %s", _buffer);
	
	ClearArray(hCategories);
	
	decl String:lang[3], String:phrase[64];
	GetLanguageInfo(GetServerLanguage(), lang, sizeof(lang));
	
	if (KvGotoFirstSubKey(kv)) {
		decl String:item[64], String:model[PLATFORM_MAX_PATH];
		do  {
			KvGetSectionName(kv, _buffer, sizeof(_buffer));
			if (!_buffer[0]) continue;
			
			if (FindStringInArray(hCategories, _buffer) == -1)
				PushArrayString(hCategories, _buffer);
			
			KvGetString(kv, lang, phrase, sizeof(phrase), "LangError");
			CategoryId category_id = Shop_RegisterCategory(_buffer, phrase, "", OnCategoryDisplay);
			
			decl symbol;
			KvGetSectionSymbol(kv, symbol);
			if (KvGotoFirstSubKey(kv)) {
				do {
					if (KvGetSectionName(kv, item, sizeof(item))) {
						KvGetString(kv, "model", model, sizeof(model));
						new pos = FindCharInString(model, '.', true);
						if (pos != -1 && StrEqual(model[pos+1], "mdl", false) && Shop_StartItem(category_id, item)) {
							PrecacheModel(model, true);
							
							KvGetString(kv, "name", _buffer, sizeof(_buffer), item);
							int price = KvGetNum(kv, "price", 5000);
							Shop_SetInfo(_buffer, "", KvGetNum(kv, "price", 5000), RoundToNearest(price*0.8), Item_Togglable, KvGetNum(kv, "duration", 86400));
							Shop_SetCallbacks(_, OnEquipItem, _, _, _, OnPreviewItem);
							
							KvJumpToKey(kv, "Attributes", true);
							Shop_KvCopySubKeysCustomInfo(kv);
							KvGoBack(kv);
							
							Shop_EndItem();
						}
					}
				} while (KvGotoNextKey(kv));
				
				KvRewind(kv);
				KvJumpToKeySymbol(kv, symbol);
			}
		} while (KvGotoNextKey(kv));
	}
	KvRewind(kv);
}

public void OnPreviewItem(int client, CategoryId category_id, const char[] category, ItemId item_id, const char[] item)
{
	if (g_bInPreview[client])
		return;
	char sCategory[64];
	Shop_GetCategoryById(category_id, sCategory, sizeof(sCategory));
	Equip(client, sCategory, true, item_id);
	//Mirror(client, buffer);
	CreateTimer(2.5, SetBackMode, GetClientUserId(client), TIMER_FLAG_NO_MAPCHANGE);
	g_bInPreview[client] = true;
	SetTP(client, true, true, 2.6);
	return;
}

public Action SetBackMode(Handle timer, any userid)
{
	CreateTimer(0.1, SpawnTimer, userid, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
}

//void Mirror(int client, const char[] sModel="")
//{
//	if (sModel[0]) {
//		SetTP(client, true, true);
//		SetEntityModel(client, sModel);
//	} else ProcessPlayer(client);
//}

public bool OnCategoryDisplay(int client, CategoryId category_id, const char[] category, const char[] name, char[] buffer, int maxlen)
{
	bool result = false;
	if (KvJumpToKey(kv, category)) {
		KvGetString(kv, g_sClLang[client], buffer, maxlen, name);
		result = true;
	}
	KvRewind(kv);
	return result;
}

public Action Command_Reload(int client, int args)
{
	OnPluginEnd();
	StartPlugin();
	OnMapStart();
	ReplyToCommand(client, "Equipments configuration successfuly reloaded!");
	return Plugin_Handled;
}

public OnMapStart()
{
	if (kv == null)
		return;

	decl String:buffer[PLATFORM_MAX_PATH];
	Shop_GetCfgFile(buffer, sizeof(buffer), "equipments_downloads.txt");
	File_ReadDownloadList(buffer);
	if (KvGotoFirstSubKey(kv)) {
		do {
			KvSavePosition(kv);
			if (KvGotoFirstSubKey(kv)) {
				do {
					KvGetString(kv, "model", buffer, sizeof(buffer));
					int iPos = FindCharInString(buffer, '.', true);
					if (iPos != -1 && StrEqual(buffer[iPos+1], "mdl", false))
						PrecacheModel(buffer, true);
				} while (KvGotoNextKey(kv));
				KvGoBack(kv);
			}
		} while (KvGotoNextKey(kv));
	}
	KvRewind(kv);

}

stock float CalculateFloatPercentage(float value1, float value2)
{
	return (value1 == 0.0 || value2 == 0.0) ?  0.0:(value1 / value2);
}

//public OnMapEnd()
//{
//	for (int i=1; i<=MAXPLAYERS; i++)
//		hTimer[i] = null;
//}

public OnClientConnected(client)
{
	if (IsFakeClient(client))
		return;
	
	hTrieEntity[client] = CreateTrie();
	hTrieItem[client] = CreateTrie();
	g_bInPreview[client] = false;
}

public OnClientPutInServer(client)
{
	//SDKHook(client, SDKHook_OnTakeDamagePost, OnTakeDamagePost);
	GetLanguageInfo(GetClientLanguage(client), g_sClLang[client], sizeof(g_sClLang[]));
}

public OnClientDisconnect(client)
{
	ProcessDequip(client);
}

public OnClientDisconnect_Post(client)
{
	if (hTrieEntity[client] != null) {
		CloseHandle(hTrieEntity[client]);
		hTrieEntity[client] = null;
	}
	if (hTrieItem[client] != null) {
		CloseHandle(hTrieItem[client]);
		hTrieItem[client] = null;
	}
	//if (hTimer[client] != null) {
	//	KillTimer(hTimer[client]);
	//	hTimer[client] = null;
	//}
}

/*public OnTakeDamagePost(victim, attacker, inflictor, Float:damage, damagetype, weapon, const Float:damageForce[3], const Float:damagePosition[3])
{
	if (!IsFakeClient(victim) && GetClientHealth(victim)-damage < 1)
	{
		if (!g_bRemoveOnDeath)
		{
			decl String:category[64], String:sModel[PLATFORM_MAX_PATH];
			for (new i = 0; i < GetArraySize(hCategories); i++)
			{
				GetArrayString(hCategories, i, category, sizeof(category));
				
				new ref = -1;
				if (!GetTrieValue(hTrieEntity[victim], category, ref))
				{
					continue;
				}
				
				new entity = EntRefToEntIndex(ref);
				if (entity != INVALID_ENT_REFERENCE && IsValidEdict(entity))
				{
					GetEntPropString(entity, Prop_Data, "m_ModelName", sModel, sizeof(sModel));
					
					decl Float:fPos[3];
					GetClientEyePosition(victim, fPos);
					fPos[2] += 100.0;
					
					new ent = CreateEntityByName("prop_physics");
					SetEntProp(ent, Prop_Data, "m_CollisionGroup", 2);
					SetEntityModel(ent, sModel);
					DispatchSpawn(ent);
					
					TeleportEntity(ent, fPos, NULL_VECTOR, damageForce);
				}
			}
		}
		ProcessDequip(victim);
	}
}*/

public Action Event_player_team(Event e, const String:name[], bool:dontBroadcast)
{
	int client = GetClientOfUserId(e.GetInt("userid"));
	if (hTrieEntity[client] == null)
		return;
	int oldteam = e.GetInt("oldteam");
	if (IsPlayerAlive(client) && e.GetInt("team") == 1 && (oldteam == 3 || oldteam == 2)) {
		ProcessDequip(client);
		g_bInPreview[client] = false;
	}
}

public Action Event_PlayerDeath(Event e, const String:name[], bool:dontBroadcast)
{
	int client = GetClientOfUserId(e.GetInt("userid"));
	if (hTrieEntity[client] == null)
		return;
	if (!g_bRemoveOnDeath) {
		decl String:category[64], String:sModel[PLATFORM_MAX_PATH];
		for (int i = 0; i < GetArraySize(hCategories); i++) {
			GetArrayString(hCategories, i, category, sizeof(category));
			
			int ref = -1;
			if (!GetTrieValue(hTrieEntity[client], category, ref))
				continue;
			
			int entity = EntRefToEntIndex(ref);
			if (entity != INVALID_ENT_REFERENCE && IsValidEdict(entity)) {
				GetEntPropString(entity, Prop_Data, "m_ModelName", sModel, sizeof(sModel));
				
				decl Float:fPos[3];
				GetClientEyePosition(client, fPos);
				
				int ent = CreateEntityByName("prop_physics");
				if (ent != -1) {
					SetEntProp(ent, Prop_Data, "m_CollisionGroup", 2);
					SetEntityModel(ent, sModel);
					
					if (!DispatchSpawn(ent))
						PrintToChatAll("Could not spawn %s", sModel);
				}
				
				TeleportEntity(ent, fPos, NULL_VECTOR, NULL_VECTOR);
			}
		}
	}
	ProcessDequip(client);
	g_bInPreview[client] = false;
}

public Event_PlayerSpawn(Event e, const String:name[], bool:dontBroadcast)
{
	CreateTimer(0.3, SpawnTimer, e.GetInt("userid"), TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
}

public Action SpawnTimer(Handle timer, any:userid)
{
	static dum[MAXPLAYERS+1];
	
	int client = GetClientOfUserId(userid);
	g_bInPreview[client] = false;
	if (!client || hTrieEntity[client] == null || IsFakeClient(client)) {
		dum[client] = 0;
		return Plugin_Stop;
	}
	
	int size = GetArraySize(hCategories);
	if (!size || dum[client] >= size) {
		dum[client] = 0;
		return Plugin_Stop;
	}
	
	decl String:category[64];
	GetArrayString(hCategories, dum[client]++, category, sizeof(category));
	Equip(client, category);
	
	return Plugin_Continue;
}

public ShopAction OnEquipItem(client, CategoryId:category_id, const String:category[], ItemId:item_id, const String:item[], bool:isOn, bool:elapsed)
{
	if (isOn || elapsed) {
		Dequip(client, category);
		RemoveFromTrie(hTrieItem[client], category);
		return Shop_UseOff;
	}
	
	Shop_ToggleClientCategoryOff(client, category_id);
	SetTrieString(hTrieItem[client], category, item);
	
	return (Equip(client, category, true)) ? Shop_UseOn:Shop_UseOff;
}

//public Action SetBackMode(Handle timer, any client)
//{
//	SetTP(client, false, true);
//	hTimer[client] = null;
//}

bool Equip(client, const String:category[], bool:from_select = false, ItemId iItem=INVALID_ITEM)
{
	if (!IsPlayerAlive(client))
		return true;
	
	Dequip(client, category);
	
	decl String:item[64];
	if (!iItem) {
		if (!GetTrieString(hTrieItem[client], category, item, sizeof(item)))
			return false;
	} else {
		if (!Shop_GetItemById(iItem, item, sizeof(item)))
			return false;
	}
	//PrintToChatAll("%s",item);

	decl Float:fAng[3], Float:fPos[3];

	decl String:entModel[PLATFORM_MAX_PATH], String:attachment[32], String:alt_attachment[32];
	entModel[0] = '\0';
	
	KvRewind(kv);
	if (KvJumpToKey(kv, category) && KvJumpToKey(kv, item)) {
		KvGetString(kv, "model", entModel, sizeof(entModel));
		if (!entModel[0]) {
			KvRewind(kv);
			return false;
		}
		
		decl String:buffer[PLATFORM_MAX_PATH];
		GetClientModel(client, buffer, sizeof(buffer));
		ReplaceString(buffer, sizeof(buffer), "/", "\\");
		if (KvJumpToKey(kv, "classes")) {
			if (KvJumpToKey(kv, buffer, false)) {
				KvGetString(kv, "attachment", attachment, sizeof(attachment), "forward");
				KvGetString(kv, "alt_attachment", alt_attachment, sizeof(alt_attachment), "");
				KvGetVector(kv, "position", fPos);
				KvGetVector(kv, "angles", fAng);
			} else {
				KvGoBack(kv);
				KvGetString(kv, "attachment", attachment, sizeof(attachment), "forward");
				KvGetString(kv, "alt_attachment", alt_attachment, sizeof(alt_attachment), "");
				KvGetVector(kv, "position", fPos);
				KvGetVector(kv, "angles", fAng);
			}
		} else {
			KvGetString(kv, "attachment", attachment, sizeof(attachment), "forward");
			KvGetString(kv, "alt_attachment", alt_attachment, sizeof(alt_attachment), "");
			KvGetVector(kv, "position", fPos);
			KvGetVector(kv, "angles", fAng);
		}
	
		if (attachment[0]) {
			if (!LookupAttachment(client, attachment)) {
				if (alt_attachment[0]) {
					if (!LookupAttachment(client, alt_attachment)) {
						PrintToChat(client, "\x04[Shop] \x01Your current model is not supported. Reason: \x04Neither attachment \"\x03%s\x04\" nor \"\x03%s\x04\" is exists on your model (%s)", attachment, alt_attachment, buffer);
						KvRewind(kv);
						return false;
					}
					strcopy(attachment, sizeof(attachment), alt_attachment);
				} else {
					PrintToChat(client, "\x04[Shop] \x01Your current model is not supported. Reason: \x04Attachment \"\x03%s\x04\" is not exists on your model (%s)", attachment, buffer);
					return false;
				}
			}
		}
	}
	KvRewind(kv);

	decl Float:or[3], Float:ang[3],
		Float:fForward[3],
		Float:fRight[3],
	Float:fUp[3];
	
	GetClientAbsOrigin(client, or);
	GetClientAbsAngles(client, ang);

	ang[0] += fAng[0];
	ang[1] += fAng[1];
	ang[2] += fAng[2];
	
	GetAngleVectors(ang, fForward, fRight, fUp);

	or[0] += fRight[0]*fPos[0] + fForward[0]*fPos[1] + fUp[0]*fPos[2];
	or[1] += fRight[1]*fPos[0] + fForward[1]*fPos[1] + fUp[1]*fPos[2];
	or[2] += fRight[2]*fPos[0] + fForward[2]*fPos[1] + fUp[2]*fPos[2];

	int iEnt = CreateEntityByName("prop_dynamic_override");
	DispatchKeyValue(iEnt, "model", 		entModel);
	DispatchKeyValue(iEnt, "spawnflags",	"256");
	DispatchKeyValue(iEnt, "solid",			"0");
	
	// We give the name for our entities here
	decl String:tName[24];
	Format(tName, sizeof(tName), "shop_equip_%d", iEnt);
	DispatchKeyValue(iEnt, "targetname", tName);
	
	DispatchSpawn(iEnt);	
	AcceptEntityInput(iEnt, "TurnOn", iEnt, iEnt, 0);
	
	SetEntPropEnt(iEnt, Prop_Send, "m_hOwnerEntity", client);
	
	SetTrieValue(hTrieEntity[client], category, EntIndexToEntRef(iEnt), true);
	
	SDKHook(iEnt, SDKHook_SetTransmit, ShouldHide);
	g_iOwner[iEnt] = client;						// записываем кто хозяин iEnt (после смерти и т.п. будет сбрасываться)
	
	TeleportEntity(iEnt, or, ang, NULL_VECTOR); 
	
	SetVariantString("!activator");
	AcceptEntityInput(iEnt, "SetParent", client, iEnt, 0);
	
	if (attachment[0]) {
		SetVariantString(attachment);
		AcceptEntityInput(iEnt, "SetParentAttachmentMaintainOffset", iEnt, iEnt, 0);
	}
	
	if (from_select && g_bPreview) 
		SetTP(client, true, true, 5.0);
	
	return true;
}

ProcessDequip(client)
{
	if (hTrieEntity[client] == null)
		return;
	
	decl String:category[64];
	for (int i=0; i<GetArraySize(hCategories); i++) {
		GetArrayString(hCategories, i, category, sizeof(category));
		Dequip(client, category);
	}
}

Dequip(client, const String:category[])
{
	int ref = -1;
	if (!GetTrieValue(hTrieEntity[client], category, ref))
		return;
	int iEnt = EntRefToEntIndex(ref);
	if (iEnt != INVALID_ENT_REFERENCE && IsValidEdict(iEnt)) {
		SDKUnhook(iEnt, SDKHook_SetTransmit, ShouldHide);
		g_iOwner[iEnt] = 0;
		AcceptEntityInput(iEnt, "Kill");
	}
	
	RemoveFromTrie(hTrieEntity[client], category);
}

public Action ShouldHide(int iEnt, int client)
{
	if (IsPlayerAlive(client)) {
		if ((g_iOwner[iEnt] == client && !IsTP(client)) || g_bInPreview[g_iOwner[iEnt]] && g_iOwner[iEnt] != client)
			return Plugin_Handled;						// чтобы от 1 лица маска не мешалась или || чтобы остальные игроки не видели шапку owner'a, которую owner смотирт в превью
	} else if (g_iOwner[iEnt] == GetEntPropEnt(client, Prop_Send, "m_hObserverTarget") && GetEntProp(client, Prop_Send, "m_iObserverMode") == 4)	//   игрок наблюдает за iOwner && если игрок в режиме просмотра от 3 лица
		return Plugin_Handled;						// когда наблюдаешь чтобы не мешалась
	
	return Plugin_Continue;
}

stock bool:LookupAttachment(client, const String:point[])
{
	if (g_hLookupAttachment==null)
		return false;
	if (client < 1 || !IsClientInGame(client))
		return false;

	return SDKCall(g_hLookupAttachment, client, point);
}

new String:_smlib_empty_twodimstring_array[][] = { { '\0' } };
stock File_AddToDownloadsTable(const String:path[], bool:recursive=true, const String:ignoreExts[][]=_smlib_empty_twodimstring_array, size=0)
{
	if (path[0] == '\0')
		return;

	if (FileExists(path)) {
		
		decl String:fileExtension[4];
		File_GetExtension(path, fileExtension, sizeof(fileExtension));
		
		if (StrEqual(fileExtension, "bz2", false) || StrEqual(fileExtension, "ztmp", false))
			return;
		
		if (Array_FindString(ignoreExts, size, fileExtension) != -1)
			return;

		AddFileToDownloadsTable(path);
		
		if (StrEqual(fileExtension, "mdl", false))
			PrecacheModel(path, true);
	} else if (recursive && DirExists(path)) {

		decl String:dirEntry[PLATFORM_MAX_PATH];
		Handle __dir = OpenDirectory(path);

		while (ReadDirEntry(__dir, dirEntry, sizeof(dirEntry))) {

			if (StrEqual(dirEntry, ".") || StrEqual(dirEntry, ".."))
				continue;
			
			Format(dirEntry, sizeof(dirEntry), "%s/%s", path, dirEntry);
			File_AddToDownloadsTable(dirEntry, recursive, ignoreExts, size);
		}
		
		CloseHandle(__dir);
	} else if (FindCharInString(path, '*', true)) {
		
		new String:fileExtension[4];
		File_GetExtension(path, fileExtension, sizeof(fileExtension));

		if (StrEqual(fileExtension, "*")) {
			decl
				String:dirName[PLATFORM_MAX_PATH],
				String:fileName[PLATFORM_MAX_PATH],
				String:dirEntry[PLATFORM_MAX_PATH];

			File_GetDirName(path, dirName, sizeof(dirName));
			File_GetFileName(path, fileName, sizeof(fileName));
			StrCat(fileName, sizeof(fileName), ".");

			new Handle:__dir = OpenDirectory(dirName);
			while (ReadDirEntry(__dir, dirEntry, sizeof(dirEntry))) {

				if (StrEqual(dirEntry, ".") || StrEqual(dirEntry, ".."))
					continue;

				if (strncmp(dirEntry, fileName, strlen(fileName)) == 0) {
					Format(dirEntry, sizeof(dirEntry), "%s/%s", dirName, dirEntry);
					File_AddToDownloadsTable(dirEntry, recursive, ignoreExts, size);
				}
			}

			CloseHandle(__dir);
		}
	}
	return;
}

stock bool File_ReadDownloadList(const String:path[])
{
	Handle file = OpenFile(path, "r");
	
	if (file  == null)
		return false;

	new String:buffer[PLATFORM_MAX_PATH];
	while (!IsEndOfFile(file)) {
		ReadFileLine(file, buffer, sizeof(buffer));
		
		int pos = StrContains(buffer, "//");
		if (pos != -1)
			buffer[pos] = '\0';
		
		TrimString(buffer);
		
		if (buffer[0] == '\0')
			continue;

		File_AddToDownloadsTable(buffer);
	}

	CloseHandle(file);
	
	return true;
}

stock File_GetExtension(const String:path[], String:buffer[], size)
{
	new extpos = FindCharInString(path, '.', true);
	
	if (extpos == -1) {
		buffer[0] = '\0';
		return;
	}

	strcopy(buffer, size, path[++extpos]);
}

stock Array_FindString(const String:array[][], size, const String:str[], bool:caseSensitive=true, start=0)
{
	if (start < 0)
		start = 0;

	for (int i=start; i < size; i++)
		if (StrEqual(array[i], str, caseSensitive))
			return i;
	
	return -1;
}

stock bool:File_GetFileName(const String:path[], String:buffer[], size)
{	
	if (path[0] == '\0') {
		buffer[0] = '\0';
		return;
	}
	
	File_GetBaseName(path, buffer, size);
	
	new pos_ext = FindCharInString(buffer, '.', true);

	if (pos_ext != -1)
		buffer[pos_ext] = '\0';
}

stock bool:File_GetDirName(const String:path[], String:buffer[], size)
{	
	if (path[0] == '\0') {
		buffer[0] = '\0';
		return;
	}
	
	new pos_start = FindCharInString(path, '/', true);
	
	if (pos_start == -1) {
		pos_start = FindCharInString(path, '\\', true);
		
		if (pos_start == -1) {
			buffer[0] = '\0';
			return;
		}
	}
	
	strcopy(buffer, size, path);
	buffer[pos_start] = '\0';
}

stock bool:File_GetBaseName(const String:path[], String:buffer[], size)
{	
	if (path[0] == '\0') {
		buffer[0] = '\0';
		return;
	}
	
	new pos_start = FindCharInString(path, '/', true);
	
	if (pos_start == -1)
		pos_start = FindCharInString(path, '\\', true);
	
	pos_start++;
	
	strcopy(buffer, size, path[pos_start]);
}